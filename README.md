[![N|Solid](https://dev2-bcito.cs57.force.com/mybcito/servlet/servlet.ImageServer?id=0150k000000GEUf&oid=00D0k000000DMJn&lastMod=1527583974000)]()

# BCITO Project

  - Code backup for BCITO
  - Master branch to be clean
  - Dev2 branch is the development branch
  - Production branch is included

# Some links

| Description | LINK |
| ------ | ------ |
| Demo `mybcito` app  | [myBcito app demo](https://aniapodhajska.bitbucket.io/blog/demo.html) |
| Jira | [Jira](https://davantimobile.atlassian.net/secure/RapidBoard.jspa?rapidView=137&projectKey=BCITO&view=planning&selectedIssue=BCITO-988) |
| Dev2 `mybcito` communities  | [Dev2 myBcito](https://dev2-bcito.cs57.force.com/mybcito) |

